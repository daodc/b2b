/**
 * Created by My PC on 09/08/2016.
 */
(function (app) {
    "use strict";
    // GLOBAL OBJECT
    app(window.jQuery, window, document);
}(function ($, window, document) {

    $('.site-lang a').on("click", function() {
        if ($(this).data('language') && $(this).data('language') != getLocale()) {
            setCookie('locale', $(this).data('language'), 30);
            window.location.reload();
        }
        return false;
    });

    //VARIABLES
    var toggle_lang = $("#toggleLang");
    var toggle_navbar = $(".navbar-toggle");
    var toggle_sidebar = $("#btnSlider");

    var lang = $(".navbar-lang");
    var search = $(".navbar-search");

    /* Input search */
    var input_search = $(".input-search");
    var search_cate_list = $(".search-cate-list");

    var swiper_cate_detail = $("#swiperCateDetail");
    var tab = $('.dropdown-menu a[data-toggle="tab"]');
    var sidebar = $('.sidebar');
    var promo_go = $(".promo-go-top");
    var offset = 200;
    var duration = 500;
    var upload_avatar = $('.avatar-block input[type=file]');
    var select_lang = $('#selectedLang');
    var modal_lang = $('#modalSelectLang');
    var lang_list = $(".pr-lang-list");
    var date_picker = $(".date-picker");
    //EVENT LISTENER
    $(function () {
        /*
         * Scroll
         */
        $(window).scroll(function() {
            if ($(window).scrollTop() > offset) {
                promo_go.fadeIn(duration);
            } else {
                promo_go.fadeOut(duration);
            }
        });
        /*
         * Promotion - go to top
         */
        promo_go.off().on("click", function(event) {
            event.preventDefault();
            scrollToTop($("body"),700);
        });
        $(document).on('click', function (e) {
            if (!$(e.target).hasClass("input-search") && !$(e.target).parents().hasClass('navbar-search')) {
                search.hide();
            }
            var dropdown = $(".dropdown-lang");
            dropdown.hide();
        });
        /*
         * Enable to click tab inside dropdown
         */
        tab.on("click", function (e) {
            e.stopPropagation();
            $(this).tab('show');
            return false;
        });
        /*
         * Toggle
         */
        toggle_lang.on("click", function () {
            lang.fadeToggle();
            if (search.is(':visible')) {
                search.hide();
            }

        });
        input_search.on("click", function (e) {
            search.fadeIn();
            if (lang.is(':visible')) {
                lang.hide();
            }
        });
        toggle_navbar.on("click", function () {
            if (!$(this).hasClass("collapsed")) {
                if (lang.is(':visible')) {
                    lang.fadeOut();
                }
                if (search.is(':visible')) {
                    search.fadeOut();
                }
            }
        });
        /*
         * Toggle sidebar
         */
        toggle_sidebar.on("click", function(){
            sidebar.fadeToggle(200);
        });
        /*
         * Update avatar
         */
        upload_avatar.on('change focus click', function(e){
            readURL(this, e);
        });
        /*
         * Select cate to search
         */
        search_cate_list.on("click", "li", function () {
            $(this).toggleClass("selected");
        });
        /*
        * COMPANY - Select language
         */
        select_lang.on("click", function () {
           modal_lang.modal("show");
        });
        modal_lang.on("click", "button", function() {
            var selectLang = lang_list.find("li.active");
            if (selectLang.length) {
                select_lang.val(selectLang.data('lang-code'));
                modal_lang.modal("hide");
            }
        });
        /*
         * Select language to update user profile
         */
        lang_list.on("click", "li", function(){
            lang_list.find("li").removeClass("active");
            $(this).toggleClass("active");
        });
        //Video gridview
        window.cateSwiper = function() {
            var loadSwiper = setInterval(function () {
                if (typeof (cateSwiper) != 'undefined') {
                    var btnNext = $("#btnNext"),
                        btnPrev = $("#btnPrev");
                    swiper_cate_detail = new Swiper('#swiperCateDetail', {
                        slidesPerView: 6,
                        slidesPerColumn: 2,
                        spaceBetween: 14,
                        breakpoints: {
                            1200: {
                                slidesPerView: 4,
                                spaceBetween: 10
                            },
                            480: {
                                slidesPerView: 2,
                                spaceBetween: 10
                            },
                        }
                    });
                    btnNext.on("click", function () {
                        swiper_cate_detail.slideNext();
                    });
                    btnPrev.on("click", function () {
                        swiper_cate_detail.slidePrev();
                    });
                    clearInterval(loadSwiper);
                }
            }, 100);


            return swiper_cate_detail;
        };
        /*
         * Date picker init
         */
        date_picker.datepicker();
    });

}));
/*
 * Scroll to Top
 */
function scrollToTop(element,time){
    $('html, body').animate({
        scrollTop: element.offset().top
    }, time);
}
/*
 * Preview image
 */
function readURL(input,event) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        var tmppath = URL.createObjectURL(event.target.files[0]);

        reader.onload = function (e) {
            $('.avatar-block .company-avatar').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}
/*
 * Read file input
 */
function readFile(input) {
    if (input.files && input.files[0]) {

        var reader = new FileReader();

        reader.onload = function(e) {
            $(".upload-select").hide();
            $(".upload-content").show();
            $(".upload-name").html(input.files[0].name);
            var size = (input.files[0].size / (1024*1024)).toFixed(2);
            $(".upload-size").html(size + " mb");
        };

        reader.readAsDataURL(input.files[0]);

    } else {
        removeUpload(input);
    }
}
/*
 * Remove file
 */
function removeUpload() {
    $(".input-hidden").replaceWith($('.input-hidden').clone());
    $('.upload-content').hide();
    $('.upload-select').show();
}