define({
    root: {
        400: "Bad Request",
        401: "Unauthorized",
        403: "Forbidden",
        404: "Not Found",
        required: "Field is required",
        invalid: "Invalid value",
        invalid_email: "Invalid email",
        login_failed: "Invalid email or password",
        inactive_account: "Inactivated account",
        request_failed: "Could not request"
    },
    ja: true
});