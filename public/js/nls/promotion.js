define({
    root: {
        page_title: "Business, we speak your language",
        select_language: "Select language",
        title_line_up: "Dubulator Video Line-up",
        categories: "Categories",
        start_here: "Start Here",
        sign_up: "Sign up",
        bottom_text: "Connect with the world using the Dubulator video edit player!<br/>If you have a promo video, upload it. The bilingual Dubulator<br/>members all over the world will translate it in your requested language.<br/>This is your chance to globalize and expand your business.<br/><br/>Apply now!",
        become_agent: "Become an Agent"
    },
    ja: true
});