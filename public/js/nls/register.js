define({
    root: {
        my_account: "MY ACCOUNT",
        email: "Email",
        company: "Company",
        password: "Password",
        confirm_password: "Confirm password",
        code: "Code",
        sign_up: "Sign up",
        tos: " By signing up, you agree to our Terms of Service and Privacy Policy",
        return_to_login_page: "Return to Login page"
    },
    ja: true
});