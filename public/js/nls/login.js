define({
    root: {
        my_account: "MY ACCOUNT",
        email: "Email",
        password: "Password",
        sign_in: "Sign in",
        forgot_password: "Forgot password?",
        dont_have_an_account: "DON'T HAVE AN ACCOUNT?",
        register: "Register"
    },
    ja: true
});