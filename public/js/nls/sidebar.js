define({
    root: {
        upload: "Upload",
        video_list: "Video list",
        company_information_setting: "Company information setting",
        security_setting: "Security setting",
        address_management: "Address management",
        contact_us: "Contact us",
        price_list: "Price list"
    },
    ja: true
});