define({
    root: {
        language: "English",
        upload: "Upload",
        login: "Login",
        logout: "Logout",
        site_language: "Site Language",
        video_language: "Video Language",
        my_account: "My account"
    },
    ja: true
});