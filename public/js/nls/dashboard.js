define({
    root: {
        upload: "Upload",
        upload_video: "Upload video",
        video_list: "Video list",
        manage_videos: "Manage videos",
        company_information_setting: "Company information setting",
        edit_company_information: "Edit company information",
        security_setting: "Security setting",
        address_management: "Address management",
        manage_address_list: "Manage address list",
        contact_us: "Contact Us",
        to_support_center: "To support center",
        to_inquire: "To inquire",
        price_list: "Price list"
    },
    ja: true
});