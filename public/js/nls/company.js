define({
    root: {
        photo: "Photo",
        language: "Language",
        company_info: "Company info",
        agency: "Agency",
        company_name: "Name",
        headquarter_address: "Headquarter address",
        country_region: "Country/Region",
        please_select: "Please select",
        zip_code: "Zip code",
        prefecture: "Prefecture",
        city: "City",
        please_input_city: "Please input city",
        address: "Address",
        person_in_charge: "Person in charge",
        email: "Email",
        input_inquiry_address: "Input inquiry address",
        tel: "TEL",
        website_address: "Website URL",
        save: "Save",
        select_file: "Select file",
        which_language: "Which language do you understand?",
        update_success: "Your information has been updated.",
        language: "Language",
        required_language: "Required language"
    },
    ja: true
});