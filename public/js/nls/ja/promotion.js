define({
    page_title: "Business, we speak your language",
    select_language: "選択された言語",
    title_line_up: "Dubulatorビデオラインアップ",
    categories: "Categories",
    start_here: "まずはここから",
    sign_up: "サインアップ",
    bottom_text: "Dubulatorは動画ひとつで世界中の人をつなぎます。</br>プロモーションビデオをお持ちなら今すぐアップロードして下さい。</br>世界中のバイリンガルDubulatorがその国の言葉を追加してくれます。</br>ビジネスが世界に広がるグローバル・チャンスです。</br></br>お申し込み・ご登録は今すぐ!!",
    become_agent: "代理店加入"
});