define({
    email: "メールアドレス",
    password: "パスワード",
    sign_in: "ログイン",
    forgot_password: "パスワードをお忘れの方",
    dont_have_an_account: "はじめての方",
    register: "新規登録"
});