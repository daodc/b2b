define({
    upload: "アップロード",
    video_list: "動画リスト",
    company_information_setting: "企業情報設定",
    security_setting: "セキュリティー設定",
    address_management: "アドレスリストを管理",
    contact_us: "お問い合わせ",
    price_list: "プライスリスト"
})