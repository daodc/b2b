define({
    email: "メールアドレス",
    company: "会社",
    password: "パスワード",
    confirm_password: "パスワードを認証する",
    code: "代理店コード",
    sign_up: "サインアップ",
    tos: " By signing up, you agree to our Terms of Service and Privacy Policy",
    return_to_login_page: "Return to Login page"
});