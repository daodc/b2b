define({
    language: "日本語",
    upload: "アップロード",
    login: "ログイン",
    logout: "ログアウト",
    site_language: "サイト言語",
    video_language: "ビデオ言語",
    my_account: "マイアカウント"
});