define({
    upload: "アップロード",
    upload_video: "動画をアップロード",
    video_list: "動画リスト",
    manage_videos: "動画を管理",
    company_information_setting: "企業情報設定",
    edit_company_information: "企業情報を編集",
    security_setting: "セキュリティー設定",
    address_management: "アドレス管理",
    manage_address_list: "アドレスリストを管理",
    contact_us: "お問い合わせ",
    to_support_center: "サポートサンターに",
    to_inquire: "問い合わせる",
    price_list: "プライスリスト"
});
