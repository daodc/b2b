define([
  'jquery',
  'underscore',
  'backbone',
  'router', // Request router.js
], function($, _, Backbone, Router){
  var initialize = function(){
    window.appViews = {};
    Router.initialize();
    $("body").addClass(getLocale().toLowerCase());
  };

  return {
    initialize: initialize
  };
});
