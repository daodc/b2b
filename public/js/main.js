require.config({
    paths: {
        jquery: 'libs/jquery/jquery-3.1.1.min',
        underscore: 'libs/underscore/underscore-1.8.3.min',
        backbone: 'libs/backbone/backbone-min',
        recaptcha: '//www.google.com/recaptcha/api.js?onload=greCaptchaCallback&render=explicit&hl='+getLocale(),
        templates: '../templates'
    },
    config: {
        i18n: {
            locale: getLocale()
        }
    }
});

require(['app'], function (App) {
    window.appViews = {};
    App.initialize();
});

function getLocale() {
    return getCookie('locale');
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length,c.length);
        }
    }
    return "";
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

window.renderRecaptcha = function(id) {
    grecaptcha.render(id, {
        'sitekey': '6LdT1QwUAAAAAPVtUFl-bdIbwpaaMAabEB-LyTx0'
    });
};

var greCaptchaCallback = function() {
    if (!document.getElementById('g-recaptcha')) {
        return;
    }
    window.renderRecaptcha('g-recaptcha');
};