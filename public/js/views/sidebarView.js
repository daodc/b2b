define([
    'jquery',
    'underscore',
    'backbone',
    'text!templates/sidebar.html',
    'i18n!nls/sidebar'
], function ($, _, Backbone, template, language) {
    var SidebarView = Backbone.View.extend({
        el: $("#mySidebar"),
        template: _.template(template),
        initialize: function() {},
        render: function() {
            this.$el.html(this.template({language: language}));
        },
        destroy: function () {
            this.undelegateEvents();
            this.$el.removeData().unbind();
            this.$el.empty();
            this.$el.hide();
        }
    });

    return SidebarView;
});