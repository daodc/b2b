define([
    'jquery',
    'underscore',
    'backbone',
    'recaptcha',
    'views/registerView',
    'text!templates/register.html',
    'i18n!nls/register'
], function ($, _, Backbone, grecaptcha, RegisterView, template, language) {
    return Backbone.View.extend({
        el: $("#router-outlet"),
        template: _.template(template),
        events: {
            "submit #register": "register",
        },
        render: function () {
            this.$el.html(this.template({language: language}));
            window.appTasks = [];
        },
        register: function (event) {
            var form = $(event.currentTarget);
            var formData = {
                email: $("input[name=email]", form),
                company: $("input[name=companyName]", form),
                password: $("input[name=pWord]", form),
                confirmPass: $("input[name=confirmPWord]", form),
                tos: $("input[name=cbPrivacy]", form)
            };
            $.ajax({
                method: "POST",
                url: "api/user/register",
                data: formData
            }).done(function( msg ) {
                    alert( "Data Saved: " + msg );
                });
            return false;
        },
        destroy: function () {
            this.undelegateEvents();
            this.$el.removeData().unbind();
            this.$el.empty();
        }
    });
});
