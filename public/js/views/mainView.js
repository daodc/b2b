define([
    'jquery',
    'underscore',
    'backbone',
    'text!templates/promotion/main.html',
    'views/promotion/videosView',
    'views/promotion/categoriesView',
    'views/promotion/bottomView',
    'i18n!nls/promotion'
], function ($, _, Backbone, template, VideosView, CategoriesView, BottomView, language) {
    return Backbone.View.extend({
        el: $("#router-outlet"),
        render: function () {
            window.appTasks = ['videos', 'categories'];
            this.$el.html(_.template(template)({language: language}));
            var videosView = new VideosView();
            videosView.render();
            var categoriesView = new CategoriesView();
            //categoriesView.render();
            categoriesView.categories.reset();
            var bottomView = new BottomView();
            bottomView.render();
        },
        destroy: function () {
            this.undelegateEvents();
            this.$el.removeData().unbind();
            this.$el.empty();
        }
    });
});
