define([
    'jquery',
    'underscore',
    'backbone',
    'collections/videos/VideosCollection',
    'text!templates/promotion/videos.html',
    'i18n!nls/promotion'
], function ($, _, Backbone, VideosCollection, template, language) {
    return Backbone.View.extend({
        render: function () {
            // Fetch promotion videos and render.
            // var videosCollection = new VideosCollection();
            // videosCollection.url = 'videos/promotion';
            // videosCollection.fetch();
            // console.log(videosCollection.toJSON());
            // End promotion videos.
            $("#promotion").before(_.template(template)({language: language}));

            setTimeout(function () {
                $( document ).ready(function() {
                    update_size();
                });
                var prMovie = $('#promo-thum ul'), items = prMovie.find('li'), len = items.length, current = 1, flashp,
                    first = items.filter(':first'), last = items.filter(':last'), triggers = $('.img-ctr'), prev = null, langset, viewflg = false;

                first.before(last.clone(true));
                last.after(first.clone(true));
                curnum(1);

                triggers.on('click', function() {
                    curnum(1);
                });

                function lineup_select(str, src){
                    curnum(str);
                    $('.ply-btn').trigger('click', str);
                    $('.thum-img').attr("src", src);
                }

                function curnum(str){
                    $('.langSet').html("");
                    if(viewflg){
                        $("object").remove( "#hippoply" );
                        viewflg = false;
                    }

                    var mainlang, mcode;
                    numArray = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16];
                    prev = str;
                    if(str == numArray[0]){ mainlang = '日本語'; mcode = 'ja'; }
                    if(str == numArray[1]){ mainlang = 'Original'; mcode = ''; }
                    if(str == numArray[2]){ mainlang = '日本語'; mcode = 'ja'; }
                    if(str == numArray[3]){ mainlang = 'English (US)'; mcode = 'en'; }
                    if(str == numArray[4]){ mainlang = 'Español'; mcode = 'es'; }
                    if(str == numArray[5]){ mainlang = '日本語'; mcode = 'ja'; }
                    if(str == numArray[6]){ mainlang = 'Español'; mcode = 'es'; }
                    if(str == numArray[7]){ mainlang = '日本語'; mcode = 'ja'; }
                    if(str == numArray[8]){ mainlang = '日本語'; mcode = 'ja'; }
                    if(str == numArray[9]){ mainlang = 'Español'; mcode = 'es'; }
                    if(str == numArray[10]){ mainlang = 'Español'; mcode = 'es'; }
                    if(str == numArray[11]){ mainlang = '日本語'; mcode = 'ja'; }
                    if(str == numArray[12]){ mainlang = '日本語'; mcode = 'ja'; }
                    if(str == numArray[13]){ mainlang = 'English (US)'; mcode = 'en'; }
                    if(str == numArray[14]){ mainlang = '日本語'; mcode = 'ja'; }
                    if(str == numArray[15]){ mainlang = 'Español'; mcode = 'es'; }

                    var t1 = '<div class="title-rating">Selected Language</div>';
                    var t2 = '<div style="position:absolute; top:60px;">';
                    var t3 = '<div class="first-lang" data-lang="' + mcode + '">' + mainlang + '</div>';
                    var t4 = '<div class="rate-block">';
                    var t5 = '<div class="rating">';
                    var t6 = '<label class="active" title="1"></label>&nbsp;';
                    var t7 = '<label class="active" title="2"></label>&nbsp;';
                    var t8 = '<label class="active" title="3"></label>&nbsp;';
                    var t9 = '<label title="4"></label>&nbsp;';
                    var t10 = '<label title="5"></label>&nbsp;';
                    var t11 = '</div></div></div>';
                    var t12 = '<div class="divider"></div>';
                    var t13 = '<div class="select-lang" data-lang="orig" style="position:absolute; top:120px; background-color:#54b1d3; border: 2px solid #54b1d3; background-color:rgba(255,255,255,0);">Original</div>';
                    var tll = t1+t2+t3+t4+t5+t6+t7+t8+t9+t10+t11+t12+t13;
                    $('.lgtop').html(tll);

                    var langno;
                    if(str == numArray[0]){ langset = ['日本語']; langcode = ['ja']; }
                    if(str == numArray[1]){ langset = []; langcode = []; }
                    if(str == numArray[2]){ langset = ['日本語','Español','Français','中文 (简体)','한국어','Italiano']; langcode = ['ja','es','fr','zh','ko','it']; }
                    if(str == numArray[3]){ langset = ['English (US)','日本語']; langcode = ['en','ja']; }
                    if(str == numArray[4]){ langset = ['Español','Français','中文 (简体)','한국어','Italiano','Tagalog']; langcode = ['es','fr','zh','ko','it','tl']; }
                    if(str == numArray[5]){ langset = ['日本語','English (US)']; langcode = ['ja','en']; }
                    if(str == numArray[6]){ langset = ['Español','Français','中文 (简体)','한국어','Italiano','Tagalog']; langcode = ['es','fr','zh','ko','it','tl']; }
                    if(str == numArray[7]){ langset = ['日本語','Español','Français','中文 (简体)','한국어','Italiano']; langcode = ['ja','es','fr','zh','ko','it']; }
                    if(str == numArray[8]){ langset = ['日本語','Español','中文 (简体)','한국어','Italiano']; langcode = ['ja','es','zh','ko','it']; }
                    if(str == numArray[9]){ langset = ['Español','中文 (简体)','한국어']; langcode = ['es','zh','ko']; }
                    if(str == numArray[10]){ langset = ['Español','Français','中文 (简体)','Italiano','Tagalog']; langcode = ['es','fr','zh','it','tl']; }
                    if(str == numArray[11]){ langset = ['Español','Français','中文 (简体)','日本語','Italiano','한국어']; langcode = ['es','fr','zh','ja','it','ko']; }
                    if(str == numArray[12]){ langset = ['日本語','Español','Français','中文 (简体)','한국어','Italiano']; langcode = ['ja','es','fr','zh','ko','it']; }
                    if(str == numArray[13]){ langset = ['日本語','English (US)']; langcode = ['ja','en']; }
                    if(str == numArray[14]){ langset = ['日本語','Español','Français','中文 (简体)','한국어','Italiano']; langcode = ['ja','es','fr','zh','ko','it']; }
                    if(str == numArray[15]){ langset = ['Español','中文 (简体)','Italiano']; langcode = ['es','zh','it']; }

                    var llen = langcode.length;
                    if(llen > 0){
                        for ( var i = 0; i < llen; i++ ) {
                            var ht = 165 + (43 * i);
                            var f1 = '<div style="position:absolute; top:' + ht + 'px;">';
                            var f2 = '<div class="select-lang" data-lang="' + langcode[i] +'">' + langset[i] + '</div>';
                            var f3 = '<div class="rate-block">';
                            var f4 = '<div class="rating">';
                            var f5 = '<label class="active" title="1"></label>&nbsp;';
                            var f6 = '<label class="active" title="2"></label>&nbsp;';
                            var f7 = '<label class="active" title="3"></label>&nbsp;';
                            var f8 = '<label title="4"></label>&nbsp;';
                            var f9 = '<label title="5"></label>&nbsp;';
                            var f10 = '</div></div></div>';
                            var fll = f1+f2+f3+f4+f5+f6+f7+f8+f9+f10;
                            $('.langSet').append(fll);
                        }

                    }else{
                        $('.langSet').append("");
                    }

                }

                var swd = null;
                $(document).on('click', '.select-lang', function(e) {
                    var disp = $(this).html();
                    $('.first-lang').html(disp);
                    var lg = $(this).data('lang');
                    flashp.changeLang(lg);
                });
                /*
                 $(document).on('click', '.first-lang', function(e) {
                 var lg = $(this).data('lang');
                 flashp.changeLang(lg);
                 });
                 */
                var timeriv, co = 0;

                $('.lineup-video').on('click', function(){
                    var vno = $('.lineup-video').index(this);
                    var img = "";
                    if(vno == 0){
                        img = "movie/2itti9QAzJ880s6M/moviepic.jpg";
                    }else if(vno == 1){
                        img = "movie/wyhV5ZAMe6Fz5bfR/moviepic.jpg";
                    }
                    if(vno >= 0){
                        var tag = $(".container-grey");
                        $('html,body').animate({'scrollTop' : tag.offset().top},1000);
                        setTimeout(function(){
                            lineup_select(vno + 1, img);
                        }, 600);

                    }
                });
                $("#swiperCateDetail").on("click", ".swiper-slide", function(){
                    var vno = $(this).data("index");
                    var src = $(this).find("img").attr("src");
                    if(vno >= 0){
                        var tag = $(".container-grey");
                        $('html,body').animate({'scrollTop' : tag.offset().top},1000);
                        setTimeout(function(){
                            lineup_select(vno + 1, src);
                        }, 600);
                    }
                });

                $('.ply-btn').on('click', function(event, v1){

                    var idx;
                    if(prev != null){
                        idx = prev;
                    }else{
                        idx = 1;
                    }

                    $(this).css('display','none');
                    $('.thum-img').css('display','none');
                    $('.bkpannel').css('display','block');


                    if(idx == numArray[0]) fvars = 'hippourl=2itti9QAzJ880s6M&tubeUrl=&stl=ja&ft=mp4&lang=ja&dubu=1100&vol=0,1,0,0&ssid=kk';
                    if(idx == numArray[1]) fvars = 'hippourl=wyhV5ZAMe6Fz5bfR&tubeUrl=&stl=ja&ft=mp4&lang=ja&dubu=0000&vol=1,0,0,0&ssid=kk';
                    if(idx == numArray[2]) fvars = 'hippourl=4NNYusJxJU2K2KN6&tubeUrl=&stl=ja,es,fr,zh,ko,it&ft=mp4&lang=ja&dubu=1110&vol=0,1,1,1&ssid=kk';
                    if(idx == numArray[3]) fvars = 'hippourl=jHi2Qi0mHSmLve3C&tubeUrl=&stl=en,ja&ft=mp4&lang=en&dubu=1110&vol=0,1,1,1&ssid=kk';
                    if(idx == numArray[4]) fvars = 'hippourl=HMiHLnjrJWukDCB6&tubeUrl=&stl=es,fr,zh,ko,it,tl&ft=mp4&lang=es&dubu=1110&vol=0,1,1,1&ssid=kk';
                    if(idx == numArray[5]) fvars = 'hippourl=bvKxsVFFESuw9hHQ&tubeUrl=&stl=ja,en&ft=mp4&lang=ja&dubu=1110&vol=0,1,1,1&ssid=kk';
                    if(idx == numArray[6]) fvars = 'hippourl=QL55G0J26gXk9vtK&tubeUrl=&stl=es,fr,zh,ko,it,tl&ft=mp4&lang=es&dubu=1110&vol=0,1,1,1&ssid=kk';
                    if(idx == numArray[7]) fvars = 'hippourl=TU5aXFgskCZttsq3&tubeUrl=&stl=ja,es,fr,zh,ko,it&ft=mp4&lang=ja&dubu=1110&vol=0,1,1,1&ssid=kk';
                    if(idx == numArray[8]) fvars = 'hippourl=3q7BGAE1pHNGuVLT&tubeUrl=&stl=ja,es,zh,ko,it&ft=mp4&lang=ja&dubu=1110&vol=0,1,1,1&ssid=kk';
                    if(idx == numArray[9]) fvars = 'hippourl=PpnrOkitMCEHpV8M&tubeUrl=&stl=es,zh,ko&ft=mp4&lang=es&dubu=1110&vol=0,1,1,1&ssid=kk';
                    if(idx == numArray[10]) fvars = 'hippourl=Ef9dWRMELUZP6D4b&tubeUrl=&stl=es,fr,zh,it,tl&ft=mp4&lang=es&dubu=1110&vol=0,1,1,1&ssid=kk';
                    if(idx == numArray[11]) fvars = 'hippourl=fe9d48d127cbf326&tubeUrl=&stl=ja,es,fr,zh,it,ko&ft=mp4&lang=ja&dubu=1111&vol=0,0.8,1,1&ssid=Annpi';
                    if(idx == numArray[12]) fvars = 'hippourl=BzbAmF98JG7GzBrK&tubeUrl=&stl=ja,es,fr,zh,ko,it&ft=mp4&lang=ja&dubu=1110&vol=0,1,1,1&ssid=kk';
                    if(idx == numArray[13]) fvars = 'hippourl=9afb58a8fdce74b3&tubeUrl=&stl=en,ja&ft=mp4&lang=en&dubu=1111&vol=0,1,1,0.4&ssid=kk';
                    if(idx == numArray[14]) fvars = 'hippourl=0iZwKYYt9MvVnhXJ&tubeUrl=&stl=ja,es,fr,zh,ko,it&ft=mp4&lang=ja&dubu=1110&vol=0,1,1,1&ssid=kk';
                    if(idx == numArray[15]) fvars = 'hippourl=FOKKbR2VyiGygsvA&tubeUrl=&stl=es,zh,it&ft=mp4&lang=es&dubu=1110&vol=0,1,1,1&ssid=kk';

                    if(!viewflg){
                        var f1 = '<object type="application/x-shockwave-flash" data="streamplay1.swf?tmn=' + new Date().getTime() + '" id="hippoply" width="100%" height="100%" align="middle">';
                        var f2 = '<param name="movie" value="streamplay1.swf?tmn=' + new Date().getTime() + '" valuetype="ref" type="application/x-shockwave-flash" />';
                        var f3 = '<param name="allowscriptaccess" value="always" />';
                        var f4 = '<param name="bgcolor" value="#000000" />';
                        var f5 = '<param name="quality" value="high" />';
                        var f6 = '<param name="salign" value="TL" />';
                        var f7 = '<param name="wmode" value="opaque" />';
                        var f8 = '<param name="allowFullScreen" value="true" />';
                        var f9 = '<param name="FlashVars" value="' + fvars + '" />';
                        var f10 = '</object>';
                        var fl = f1+f2+f3+f4+f5+f6+f7+f8+f9+f10;
                        $('.player-guide').append(fl);
                        flashp = $('#hippoply').get(0);
                        viewflg = true;
                    }

                });

                function movie_end(str){
                    if(viewflg){
                        $("object").remove("#hippoply");
                        $('.ply-btn').css('display','block');
                        $('.thum-img').css('display','block');
                        viewflg = false;
                    }
                }
                $(window).on("resize", function(e){
                    update_size();
                    e.preventDefault()
                });
                function update_size(){
                    var parent = $(".video-frm");
                    var frame = $(".promo-frm");
                    var video_area = $(".promo-frm .parea");
                    var rating = $('.rating-blk');
                    var video_cate = $('.video-cate');

                    var wd, adj, scale;

                    wd = parent.width();

                    adj = wd - 362;
                    scale = parseFloat(678/395); // = default width / default height

                    if(wd > 720){
                        var video_height = parseFloat(adj / scale);

                        frame.css({"min-height" : video_height});
                        video_area.width(adj);
                        video_area.height(video_height);

                        rating.css({"position": "absolute", "left": adj + 20, "height":parseFloat(adj / scale), "top": 0, "width": 320 });
                    }
                    else{
                        var full = wd - 40;
                        var video_height = parseFloat(full / scale);

                        frame.css({"min-height" : video_height + 340});
                        video_area.width(full);
                        video_area.height(video_height);
                        video_cate.css({"margin-top" : 275});
                        rating.css({"position": "absolute", "left": 20, "top": parseFloat(full / scale), "height": 270, "width":full });
                    }
                }

                // End of copy
                window.appTasks = _.without(window.appTasks, 'videos');
            }, 100);
            return this;
        }
    });
});
