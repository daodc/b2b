define([
    'jquery',
    'underscore',
    'backbone',
    'collections/categories/CategoriesCollection',
    'views/promotion/categoryView',
    'text!templates/promotion/categories.html',
    'i18n!nls/promotion'
], function ($, _, Backbone, CategoriesCollection, CategoryView, template, language) {
    return Backbone.View.extend({
        tagName: "ul",
        template: _.template(template),
        categories: new CategoriesCollection(),
        initialize: function() {
            this.$el.addClass('col-xs-12 ist-inline list-unstyled cate-list');
            $("#promotion").append(this.template({language: language}));
            this.listenTo(this.categories, 'add', this.addOne);
            this.listenTo(this.categories, 'reset', this.addAll);
            this.listenTo(this.categories, 'all', this.render);

            this.categories.fetch();
            window.appTasks = _.without(window.appTasks, 'categories');
            return this;
        },
        render: function() {
            $("#categories").html(this.el);
            return this;
        },
        addOne: function(category) {
            var view = new CategoryView({model: category});
            this.$el.append(view.render().el);
        },
        addAll: function() {
            this.categories.each(this.addOne, this);
        }
    });
});
