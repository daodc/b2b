define([
    'jquery',
    'underscore',
    'backbone',
    'text!templates/promotion/category.html'
], function ($, _, Backbone, template) {
    return Backbone.View.extend({
        tagName: 'li',
        template: _.template(template),
        render: function() {
            this.$el.addClass('col-xs-6 col-sm-3 col-md-2 col-lg-14-2').append(this.template(this.model.toJSON()));
            this.delegateEvents();
            return this;
        }
    });
});
