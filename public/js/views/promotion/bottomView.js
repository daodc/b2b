define([
    'jquery',
    'underscore',
    'backbone',
    'text!templates/promotion/bottom.html',
    'i18n!nls/promotion'
], function ($, _, Backbone, template, language) {
    return Backbone.View.extend({
        initialize: function() {},
        render: function() {
            $("#promotion").after(_.template(template)({language: language}));
        }
    });
});
