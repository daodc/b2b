define([
    'jquery',
    'underscore',
    'backbone',
    'recaptcha',
    'views/dashboardView',
    'text!templates/dashboard.html',
    'i18n!nls/dashboard'
], function ($, _, Backbone, grecaptcha, DashboardView, template, language) {
    return Backbone.View.extend({
        el: $("#router-outlet"),
        template: _.template(template),
        render: function () {
            this.$el.html(this.template({language: language}));
            window.appTasks = [];
        },
        destroy: function () {
            this.undelegateEvents();
            this.$el.removeData().unbind();
            this.$el.empty();
        }
    });
});
