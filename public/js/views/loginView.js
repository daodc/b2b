define([
    'jquery',
    'underscore',
    'backbone',
    'recaptcha',
    'views/loginView',
    'text!templates/login.html',
    'i18n!nls/login'
], function ($, _, Backbone, grecaptcha, LoginView, template, language) {
    return Backbone.View.extend({
        el: $("#router-outlet"),
        template: _.template(template),
        events: {
            "submit .login-form": "login"
        },
        render: function () {
            this.$el.html(this.template({language: language}));
            window.appTasks = [];
        },
        destroy: function () {
            this.undelegateEvents();
            this.$el.removeData().unbind();
            this.$el.empty();
        },
        login: function (e) {
            var self = this;
            e.preventDefault();
            $.ajax({
                method: "POST",
                url: "api/user/login",
                data: {username: $("input[name=email]").val(), password: $("input[name=password]").val()},
            }).done(function (data) {
                if (typeof data.error != "undefined") {
                    require(['i18n!nls/errors'], function (errors) {
                        if ($(".login-form", self.$el).find('.text-danger').length) {
                            $(".login-form .text-danger", self.$el).html(errors[data.error.code]);
                        } else {
                            $(".login-form", self.$el).prepend('<div class="text-danger">'+errors[data.error.code]+'</div>');
                        }
                    });
                } else if(data.id) {
                    var views = [];
                    _.each(appViews, function(view, name) {
                        if (typeof view.updateAuth == 'function') {
                            views.push(name);
                        }
                    });
                    if (views.length) {
                        views.forEach(function(name, index) {
                            appViews[name].updateAuth();
                            if (index == (views.length-1)) {
                                window.location.href = "#dashboard";
                            }
                        });
                    } else {
                        window.location.href = "#dashboard";
                    }
                }
            });
        }
    });
});
