define([
    'jquery',
    'underscore',
    'backbone',
    'text!templates/footer.html',
    'i18n!nls/footer'
], function ($, _, Backbone, template, language) {
    var FooterView = Backbone.View.extend({
        el: $("#footer"),
        template: _.template(template),
        initialize: function() {},
        render: function() {
            this.$el.html(this.template({language: language}));
        },
        destroy: function () {
            this.undelegateEvents();
            this.$el.removeData().unbind();
            this.$el.empty();
        }
    });

    return FooterView;
});
