define([
    'jquery',
    'underscore',
    'backbone',
    'views/companyView',
    'text!templates/company.html',
    'i18n!nls/company'
], function ($, _, Backbone, CompanyView, template, language) {
    return Backbone.View.extend({
        el: $("#router-outlet"),
        template: _.template(template),
        events: {
            "submit #infoTab form": "updateCompanyInfo",
            "submit #picTab form": "uploadPhoto",
            "submit #languageTab form": "updateLanguage"
        },
        render: function () {
            var self = this;
            $.ajax({
                method: "GET",
                url: "api/company/information",
                data: {
                    name: $("input[name=name]").val(),
                    address: $("input[name=address]").val(),
                    owner: $("input[name=owner]").val(),
                    email: $("input[name=email]").val(),
                    tel: $("input[name=tel]").val(),
                    url: $("input[name=url]").val()
                },
                error: function() {
                    alert("Could not connect to api server.");
                }
            }).done(function (data) {
                self.$el.html(self.template({info: data, language: language}));
                window.appTasks = [];
                $('select[name=language] option').each(function() {
                    if ($(this).attr("value") == data.language) {
                        $(this).attr('selected', 'selected');
                    } else {
                        $(this).removeAttr('selected');
                    }
                });
            });
            $.ajax({
                method: "GET",
                url: "api/company/photo",
                error: function() {
                    alert("Could not connect to api server.");
                }
            }).done(function (data) {
                $("img.company-avatar", this.$el).attr('src', data.uri);
            });
        },
        updateCompanyInfo: function(e) {
            e.preventDefault();
            var method = $("input[name=id]").val() ? 'PATCH' : 'POST';
            $.ajax({
                method: method,
                url: "api/company/information",
                data: {
                    id: $("input[name=id]").val(),
                    name: $("input[name=name]").val(),
                    address: $("input[name=address]").val(),
                    owner: $("input[name=owner]").val(),
                    email: $("input[name=email]").val(),
                    tel: $("input[name=tel]").val(),
                    url: $("input[name=url]").val()
                }
            }).done(function (data) {
                if (typeof data.id != 'undefined' && data.id) {
                    alert(language.update_success);
                }
            });
        },
        uploadPhoto: function(e) {
            e.preventDefault();
            var formData = new FormData();
            formData.append('upload', $('input[name=upload]')[0].files[0]);
            formData.append('ext', $('input[name=upload]')[0].files[0].name.split('.').pop() || '');

            $.ajax({
                url: 'api/company/photo',
                type: 'POST',
                data: formData,
                processData: false,  // tell jQuery not to process the data
                contentType: false,  // tell jQuery not to set contentType
                success: function(data) {
                    if (data.status = 1) {
                        alert(language.update_success);
                    }
                }
            });
        },
        updateLanguage: function(e) {
            e.preventDefault();
            var formData = new FormData();
            formData.append('language', $('select[name=language]').val());

            $.ajax({
                url: 'api/company/language',
                type: 'POST',
                data: formData,
                processData: false,  // tell jQuery not to process the data
                contentType: false,  // tell jQuery not to set contentType
                success: function(data) {
                    if (data.status = 1) {
                        alert(language.update_success);
                    } else {
                        alert(language[data.message]);
                    }
                }
            });
        },
        destroy: function () {
            this.undelegateEvents();
            this.$el.removeData().unbind();
            this.$el.empty();
        }
    });
});
