define([
    'jquery',
    'underscore',
    'backbone',
    'recaptcha',
    'views/uploadView',
    'text!templates/upload.html',
    'i18n!nls/upload'
], function ($, _, Backbone, grecaptcha, DashboardView, template, language) {
    return Backbone.View.extend({
        el: $("#router-outlet"),
        template: _.template(template),
        render: function () {
            this.$el.html(this.template({language: language}));
            window.appTasks = [];
        },
        destroy: function () {
            this.undelegateEvents();
            this.$el.removeData().unbind();
            this.$el.empty();
        }
    });
});
