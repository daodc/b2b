define([
    'jquery',
    'underscore',
    'backbone',
    'text!templates/header.html',
    'i18n!nls/header'
], function ($, _, Backbone, template, language) {
    var HeaderView = Backbone.View.extend({
        el: $("#header"),
        template: _.template(template),
        initialize: function() {},
        render: function() {
            this.$el.html(this.template({language: language}));
            this.updateAuth();
        },
        destroy: function () {
            this.undelegateEvents();
            this.$el.removeData().unbind();
            this.$el.empty();
        },
        updateAuth: function() {
            if (getCookie('isLogged') == 'true') {
                $('.login-button', this.$el).addClass('hidden');
                $('.logout-button', this.$el).removeClass('hidden');
            } else {
                $('.login-button', this.$el).removeClass('hidden');
                $('.logout-button', this.$el).addClass('hidden');
            }
        }
    });

    return HeaderView;
});
