define([
    'jquery',
    'underscore',
    'backbone',
    'models/category/CategoryModel'
], function($, _, Backbone, CategoryModel){
    return Backbone.Collection.extend({
        model: CategoryModel,
        url: "api/categories.taf"
    });
});
