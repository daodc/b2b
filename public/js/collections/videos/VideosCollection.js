define([
    'jquery',
    'underscore',
    'backbone',
    'models/video/VideoModel'
], function($, _, Backbone, VideoModel){
    return Backbone.Collection.extend({
        model: VideoModel,
        url: "videos"
    });
});
