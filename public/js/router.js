define([
    'jquery',
    'underscore',
    'backbone',
    'views/mainView',
    'views/headerView',
    'views/footerView',
    'views/sidebarView'
], function ($, _, Backbone, MainView, HeaderView, FooterView, SidebarView) {
    var AppRouter = Backbone.Router.extend({
        routes: {
            // Define routes
            'login': 'loginAction',
            'logout': 'logoutAction',
            'register': 'registerAction',
            'dashboard': 'dashboardAction',
            'company': 'companyAction',
            'upload': 'uploadAction',
            // Default
            '*actions': 'defaultAction'
        },
        execute: function(callback, args, name) {
            var isLogged = getCookie('isLogged') || false;
            if (isLogged == 'true') {
                // Is logged in.
                $("body").addClass('logged-in');
                if (_.indexOf(['defaultAction', 'loginAction'], name) != -1) {
                    window.location.href = '#dashboard';
                    return false;
                }
                if (typeof appViews['sidebar'] == 'undefined') {
                    var sidebarView = new SidebarView();
                    sidebarView.render();
                    appViews['sidebar'] = sidebarView;
                }
            } else {
                if (typeof appViews['sidebar'] != 'undefined') {
                    appViews['sidebar'].destroy();
                }
                // Logout
                $("body").removeClass('logged-in');
                if (_.indexOf(['loginAction', 'registerAction', 'defaultAction'], name) == -1) {
                    window.location.href = '#login';
                    return false;
                }
            }
        }
    });

    var initialize = function () {
        var appRouter = new AppRouter;
        // Default
        appRouter.on('route:defaultAction', function (actions) {
            var mainView = new MainView();
            mainView.render();
            appViews['home'] = mainView;
            var loadSwiper = setInterval(function () {
                if (typeof (cateSwiper) != 'undefined') {
                    cateSwiper();
                    clearInterval(loadSwiper);
                }
            }, 100);
            activePage("home");
        });
        // Login page.
        appRouter.on('route:loginAction', function (actions) {
            require(['views/loginView'], function (LoginView) {
                var loginView = new LoginView();
                loginView.render();
                appViews['login'] = loginView;
            });
            activePage("login");
        });
        // Logout page.
        appRouter.on('route:logoutAction', function (actions) {
            $.ajax({
                method: "POST",
                url: "api/user/logout",
                error: function(request, status, error) {
                    require(['i18n!nls/errors'], function (errors) {
                        alert(errors.request_failed);
                        window.location = "";
                    });
                }
            }).done(function() {
                _.each(appViews, function(view) {
                   if (typeof view.updateAuth == 'function') {
                       view.updateAuth();
                   }
                });
                window.location.href = '#login';
            });
        });
        // Register page.
        appRouter.on('route:registerAction', function (actions) {
            require(['views/registerView'], function (RegisterView) {
                var registerView = new RegisterView();
                registerView.render();
                appViews['register'] = registerView;
            });
            activePage("register");
        });
        // Dashboard
        appRouter.on('route:dashboardAction', function (actions) {
            require(['views/dashboardView'], function (DashboardView) {
                var dashboardView = new DashboardView();
                dashboardView.render();
                appViews['dashboard'] = dashboardView;
            });
            activePage("dashboard");
        });

        appRouter.on('route:companyAction', function (actions) {
            require(['views/companyView'], function (CompanyView) {
                var companyView = new CompanyView();
                companyView.render();
                appViews['company'] = companyView;
            });
            activePage('company');
        });

        appRouter.on('route:uploadAction', function (actions) {
            require(['views/uploadView'], function (UploadView) {
                var uploadView = new UploadView();
                uploadView.render();
                appViews['upload'] = uploadView;
            });
            activePage('upload');
        });

        var headerView = new HeaderView();
        headerView.render();
        appViews['header'] = headerView;
        var footerView = new FooterView();
        footerView.render();
        appViews['footer'] = footerView;

        if (!Backbone.History.started) {
            Backbone.history.start();
        }

        function activePage(page) {
            $('body')[0].className = $('body')[0].className.replace(/\bpage.*?\b/g, '');
            $("body").removeClass('home').addClass((page == 'home') ? page : "page-" + page);
        }
    };
    return {
        initialize: initialize
    };
});
